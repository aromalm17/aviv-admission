const express = require('express');
const session = require('express-session');
const dotenv = require('dotenv').config();
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const path = require('path');
var cors = require('cors');

const pool = require('./config/database');

passport.use(new GoogleStrategy({
  clientID: process.env.ID,
  clientSecret: process.env.KEY,
  callbackURL: 'http://localhost:3000/auth/google/callback',
},
  (accessToken, refreshToken, profile, done) => {
    if (profile.emails && profile.emails.length > 0) {
      const email = profile.emails[0].value;
      const query = 'SELECT * FROM users WHERE email = ?';

      pool.query(query, [email], (error, results) => {

        if (results.length > 0) {
          console.log("Email already registered");
        }

        const insertQuery = 'INSERT INTO users (email) VALUES (?)';
        pool.query(insertQuery, [email], (insertError) => {
          if (insertError) {
            return done(insertError);
          }

          return done(null, profile);
        });
      });
    }
  }));

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((obj, done) => {
  done(null, obj);
});

const app = express();
const port = 3000;
app.use(cors())
app.use(session({
  secret: 'gvghvgrv',
  resave: true,
  saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set('view engine', 'ejs');
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use('/auth', require('./routes/authRoute')(passport));
app.use('/', require('./routes/routes'));

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});