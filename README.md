Title: Aviv Digital Institute Admission Portal

Description:
The project aims to create an efficient admission portal for Aviv Digital Institute featuring Google login integration, profile image upload, and personal details submission for streamlined application processing.
