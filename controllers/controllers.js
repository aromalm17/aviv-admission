const pool = require('../config/database');
const moment = require('moment');


async function insert(req, res) {
    const {
        first_name, last_name, gender, date_of_birth,
        email, phone_number, course, mode_of_training, address,
        qualification, school, pincode, district, city
    } = req.body;

    const register_number = req.params.register_number;
    const avatar = req.file;

    if (!avatar) {
        return res.status(400).json({ success: false, error: 'Profile photo is required' });
    }

    const now = new Date();
    const admission_date = now.toISOString().split('T')[0];

    const connection = pool.promise();

    try {
        const query = `
            INSERT INTO students
            (register_number, avatar, first_name, last_name, gender, date_of_birth, email, phone_number, course, mode_of_training, admission_date, address, qualification, school, pincode, district, city)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        `;

        const values = [
            register_number,
            avatar.filename,
            first_name, last_name, gender, date_of_birth,
            email, phone_number, course, mode_of_training, admission_date, address,
            qualification, school, pincode, district, city
        ];

        await connection.query(query, values);

        const studentData = {
            register_number,
            avatar: avatar.filename,
            first_name, last_name, gender, date_of_birth,
            email, phone_number, course, mode_of_training, admission_date, address,
            qualification, school, pincode, district, city,
        };

        res.render('view.ejs', studentData);
    } catch (error) {
        console.error('Error inserting into MySQL:', error);
        res.status(500).json({ success: false, error: 'Internal Server Error' });
    }
}

async function update(req, res) {
    try {
        const { first_name, last_name, date_of_birth, email, phone_number, address, course, qualification, mode_of_training, school, pincode, district, city, gender } = req.body;
        const register_number = req.params.register_number;
        const avatar = req.file;

        const selectQuery = 'SELECT avatar FROM students WHERE register_number = ?';
        const [selectResult] = await pool.promise().query(selectQuery, [register_number]);
        const existingAvatarFileName = selectResult[0].avatar;

        const avatarFilename = avatar ? avatar.filename : existingAvatarFileName;

        const updateQuery = 'UPDATE students SET first_name = ?, last_name = ?, date_of_birth = ?, phone_number = ?, gender = ?, address = ?, course = ?, qualification = ?, mode_of_training = ?, school = ?, pincode = ?, district = ?, city = ?, avatar = ? WHERE register_number = ?';

        const [result] = await pool.promise().query(updateQuery, [first_name, last_name, date_of_birth, phone_number, gender, address, course, qualification, mode_of_training, school, pincode, district, city, avatarFilename, register_number]);

        const now = new Date();
        const admission_date = now.toISOString().split('T')[0];

        if (result.affectedRows === 1) {
            res.render('view.ejs', {
                register_number: register_number,
                avatar: avatarFilename,
                first_name: first_name,
                last_name: last_name,
                gender: gender,
                date_of_birth: date_of_birth,
                email: email,
                phone_number: phone_number,
                course: course,
                mode_of_training: mode_of_training,
                admission_date: admission_date,
                address: address,
                qualification: qualification,
                school: school,
                pincode: pincode,
                district: district,
                city: city,
            });
        } else {
            console.error('Update failed:', result);
            res.status(500).json({ success: false, error: 'Update failed' });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, error: 'Internal Server Error' });
    }
}

async function edit(req, res) {
    try {
        const registerNumber = req.params.register_number;

        const query = 'SELECT * FROM students WHERE register_number = ?';
        const [existingStudentData] = await pool.promise().query(query, [registerNumber]);

        if (!existingStudentData.length) {
            return res.status(404).json({ success: false, error: 'Student not found' });
        }

        const formattedDateOfBirth = moment(existingStudentData[0].date_of_birth).format('YYYY-MM-DD');

        res.render('update.ejs', {
            register_number: existingStudentData[0].register_number,
            avatar: existingStudentData[0].avatar,
            first_name: existingStudentData[0].first_name,
            last_name: existingStudentData[0].last_name,
            date_of_birth: formattedDateOfBirth,
            email: existingStudentData[0].email,
            gender: existingStudentData[0].gender,
            mode_of_training: existingStudentData[0].mode_of_training,
            phone_number: existingStudentData[0].phone_number,
            address: existingStudentData[0].address,
            qualification: existingStudentData[0].qualification,
            school: existingStudentData[0].school,
            course: existingStudentData[0].course,
            pincode: existingStudentData[0].pincode,
            district: existingStudentData[0].district,
            city: existingStudentData[0].city,
        });

    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, error: 'Internal Server Error' });
    }
}

async function success(req, res) {
    try {
        const registerNumber = req.params.register_number;

        const query = 'SELECT * FROM students WHERE register_number = ?';
        const [studentData] = await pool.promise().query(query, [registerNumber]);

        if (!studentData.length) {
            return res.status(404).json({ success: false, error: 'Student not found' });
        }

        const insertQuery = `
            INSERT INTO main
            (register_number, avatar, first_name, last_name, gender, date_of_birth, email, phone_number, course, mode_of_training, admission_date, address, qualification, school, pincode, district, city)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        `;

        const values = [
            studentData[0].register_number,
            studentData[0].avatar,
            studentData[0].first_name,
            studentData[0].last_name,
            studentData[0].gender,
            studentData[0].date_of_birth,
            studentData[0].email,
            studentData[0].phone_number,
            studentData[0].course,
            studentData[0].mode_of_training,
            studentData[0].admission_date,
            studentData[0].address,
            studentData[0].qualification,
            studentData[0].school,
            studentData[0].pincode,
            studentData[0].district,
            studentData[0].city,
        ];

        await pool.promise().query(insertQuery, values);

        res.send("Done");
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, error: 'Internal Server Error' });
    }
}


module.exports = { insert, update, edit, success }