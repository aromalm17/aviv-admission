const express = require("express");
const router = express.Router();
const controller = require("../controllers/controllers");
const multer = require('multer');

const upload = multer({ dest: 'uploads/' });

router.get('/index', (req, res) => {
  const email = req.query.email;
  function randomNumber() {
    return Math.floor(100000 + Math.random() * 900000);
  }
  const register_number = randomNumber();

  res.render('index.ejs', { email, register_number });
});

router.get('/', (req, res) => {
  res.render('splashScreen.ejs');
});

router.post('/insert/:register_number', upload.single('avatar'), controller.insert);
router.post('/update/:register_number', upload.single('avatar'), controller.update);
router.get('/edit/:register_number', controller.edit);
router.get('/success/:register_number', controller.success);

module.exports = router;