const express = require('express');
const router = express.Router();

module.exports = (passport) => {
  router.get('/google',
    passport.authenticate('google', { scope: ['profile', 'email'] }));

  router.get('/google/callback',
    passport.authenticate('google', { failureRedirect: '/' }),
    (req, res) => {
      res.redirect(`/index?email=${req.user.emails[0].value}`);
    });

  return router;
};